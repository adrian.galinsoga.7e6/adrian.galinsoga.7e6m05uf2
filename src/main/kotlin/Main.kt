/**
 *  Programa de manteniment i neteja de les dades dels treballadors de DAM1 SA
 *
 *  @author Adrián Galinsoga
 */
import java.time.LocalDate

/*
 * Llista que guarda les dades dels treballadors
 * En aquest ordre, conté la informació seguent:
 * Nom, edat, dni, nº mòvil, adreça, usuari actiu
*/
var workersData = mutableListOf(
    mutableListOf("Jordi", "04/06/1988", "33842962A", "648852357", "jordi@ITB2022", "true"),
    mutableListOf("Maria", "07/07/1999", "01637101F", "687957824", "Marieta23,", "true"),
    mutableListOf("Nadia", "24/02/1966", "40886594T", "628987423", "HugoPau1966*", "true"),
    mutableListOf("Josep", "18/12/1939", "65548290S", "635859146", "qwerty123!", "true"),
    mutableListOf("Joana", "11/11/1989", "89766736J", "698741258", "JoAnA1989||*||", "true"),
    mutableListOf("Ricardo", "15/06/1949", "97597325E", "698753214", "Ricardo1949", "true"),
    mutableListOf("Sara", "08/09/1980", "86480629P", "654632687", "saramartin123", "true"),
    mutableListOf("Olga", "30/04/1996", "50671444Y", "658982346", "U!gu3tt4", "true"),
    mutableListOf("Samanta", "01/11/1975", "70135386Z", "666932156", "Sami45-", "true"),
    mutableListOf("Julia", "16/05/1942", "74790195Z", "621438719", "JuliaMari1917?", "true"))

/*
 * Llista de relació entre número DNI i la seva lletra
 */
val dniLetter = listOf<Char>('T', 'R', 'W', 'A', 'G', 'M', 'Y',	'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H',	'L', 'C', 'K', 'E')


/**
 * Funció que cerca a la llista de treballadors quins usuaris tenen dades erronies
 * o estan jubilats i els desactiva el compte
 *
 * @param workersData Llista que guarda les dades dels treballadors
 */
fun checkData(workersData: MutableList<MutableList<String>>) {
    for (i in 0..workersData.lastIndex) {
        if (isRetired(workersData[i][1]) && !isValidDni(workersData[i][2]) && !isValidPhone(workersData[i][3]) && !isValidPassword(workersData[i][4])) {
            workersData[i][6] = "false"
        }
    }
}


fun isValidPhone(telphNumber: String): Boolean {
    if (telphNumber.length != 9) return false
    for (number in telphNumber) {
        if (number !in '0'.. '9') return false
    }
    return true
}

fun isValidPassword(password: String): Boolean {
    var lowerCase = false
    var upperCase = false
    var digit = false
    var symbol = false
    if (password.length < 8) return false
    for (character in password) {
        if (character in 'a'..'z') lowerCase = true
        if (character in 'A'..'Z') upperCase = true
        if (character in '0'..'9') digit = true
        if (character in '!'..'/' || character in ':'..'@' || character in '['..'`' || character in '{'..'~') symbol = true
    }

    return lowerCase && upperCase && digit && symbol
}

fun isRetired(date: String): Boolean {
    val yearOfBirth = date.split("/")[2].toInt()
    val monthOfBirth = date.split("/")[1].toInt()
    val dayOfBirth = date.split("/")[0].toInt()
    if (yearOfBirth > LocalDate.now().year - 65) return false
    else if (yearOfBirth == LocalDate.now().year - 65 && monthOfBirth > LocalDate.now().monthValue) return false
    else if (yearOfBirth == LocalDate.now().year - 65 && monthOfBirth == LocalDate.now().monthValue && dayOfBirth > LocalDate.now().dayOfMonth) return false

    return true
}

fun isValidDni(dni: String): Boolean {
    if (dni.length != 9) return false
    if (dni.dropLast(1).toIntOrNull() == null) return false
    if (dni.last() !in 'A'..'Z') return false
    return dniLetter[dni.dropLast(1).toInt() % 23] == dni.last()
}

fun main() {
    println("Benvinguts a l'aplicació de gestió de dades personals dels treballadors de DAM1 SA")
    println("La situació actual dels treballadors registrats al sistema és la següent:")
    println("Nombre de treballadors a la base de dades: ${workersData.size}")
    println("Informació dels treballadors")
    for (i in 1..workersData.lastIndex) {
        println("Nom: ${workersData[i][0]} Data Naixement: ${workersData[i][1]} DNI: ${workersData[i][2]} Telèfon: ${workersData[i][3]} Password: ${workersData[i][4]} Actiu?: ${workersData[i][5]}")
    }
    println("Procedim a desactivar els usuaris amb dades incorrectes o jubilats")

    checkData(workersData)

    println("Usuaris als que s'ha d'avisar: ")
    for (worker in workersData) {
        if (worker[5] == "false") println("Nom: ${worker[0]}")
    }
}