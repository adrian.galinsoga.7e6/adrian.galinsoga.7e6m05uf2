import org.junit.jupiter.api.Assertions.*

internal annotation class isValidPassword
@isValidPassword
fun isValidPassword(password: String): Boolean {
    var lowerCase = false
    var upperCase = false
    var digit = false
    var symbol = false
    if (password.length < 8) return false
    for (character in password) {
        if (character in 'a'..'z') lowerCase = true
        if (character in 'A'..'Z') upperCase = true
        if (character in '0'..'9') digit = true
        if (character in '!'..'/' || character in ':'..'@' || character in '['..'`' || character in '{'..'~') symbol = true
    }

    return lowerCase && upperCase && digit && symbol
}
