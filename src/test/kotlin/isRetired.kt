import org.junit.jupiter.api.Assertions.*
import java.time.LocalDate

internal annotation class isRetired
@isRetired
fun isRetired(date: String): Boolean {
    val yearOfBirth = date.split("/")[2].toInt()
    val monthOfBirth = date.split("/")[1].toInt()
    val dayOfBirth = date.split("/")[0].toInt()
    if (yearOfBirth > LocalDate.now().year - 65) return false
    else if (yearOfBirth == LocalDate.now().year - 65 && monthOfBirth > LocalDate.now().monthValue) return false
    else if (yearOfBirth == LocalDate.now().year - 65 && monthOfBirth == LocalDate.now().monthValue && dayOfBirth > LocalDate.now().dayOfMonth) return false

    return true