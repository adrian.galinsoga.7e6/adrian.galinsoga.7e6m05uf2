import org.junit.jupiter.api.Assertions.*

internal annotation class isValidDni
@isValidDni
fun isValidDni(dni: String): Boolean {
    if (dni.length != 9) return false
    if (dni.dropLast(1).toIntOrNull() == null) return false
    if (dni.last() !in 'A'..'Z') return false
    return dniLetter[dni.dropLast(1).toInt() % 23] == dni.last()
}
